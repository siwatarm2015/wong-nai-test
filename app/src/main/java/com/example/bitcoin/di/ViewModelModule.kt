package com.example.bitcoin.di

import com.example.bitcoin.data.datasource.DataSource
import com.example.bitcoin.data.datasource.RemoteDataSource
import com.example.bitcoin.presentation.ui.main.MainViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val viewModelModule = module {
    viewModel { MainViewModel(get())}
    single<DataSource>(createdAtStart = true){ RemoteDataSource(get()) }
}