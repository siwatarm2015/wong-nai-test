package com.example.bitcoin.di

import android.annotation.SuppressLint
import android.provider.Settings
import com.chuckerteam.chucker.api.ChuckerInterceptor
import com.example.bitcoin.BuildConfig.BASE_URL
import com.example.bitcoin.data.datasource.DataSource
import com.example.bitcoin.data.datasource.RemoteDataSource
import com.example.bitcoin.data.repository.CoinRepository
import com.example.bitcoin.data.service.ApiService
import com.example.bitcoin.domain.usecase.GetCoinUseCase
import com.example.bitcoin.presentation.ui.main.MainViewModel
import com.google.gson.GsonBuilder
import com.orhanobut.logger.Logger.i
import com.orhanobut.logger.Logger.json
import okhttp3.logging.HttpLoggingInterceptor
import org.json.JSONException
import org.json.JSONObject
import org.koin.android.ext.koin.androidContext
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.module.Module
import org.koin.core.qualifier.named
import org.koin.dsl.module
import retrofit2.Converter
import retrofit2.converter.gson.GsonConverterFactory

val appModule: Module = module {
    single { GsonBuilder().serializeNulls().create()!! }
}

@SuppressLint("HardwareIds")
val networkModule = module {

    single {
        OkHttpBuilder(
            get(),
            get(),
            get(),
            androidContext()
        ).build()
    }

    single<String>(named("uniqueID")) {
        Settings.Secure.getString(
            androidContext().contentResolver,
            Settings.Secure.ANDROID_ID
        )
    }


    single<Converter.Factory> { GsonConverterFactory.create() }

    single { DefaultRequestInterceptor() }

    single { ChuckerInterceptor.Builder(androidContext())
        .maxContentLength(50000L).build() }

    single {

        HttpLoggingInterceptor { message ->
            try {
                JSONObject(message)
                json(message)
            } catch (error: JSONException) {
                i(message)
            }
        }.apply { level = HttpLoggingInterceptor.Level.BODY }

    }

    single<ApiService>(named("retrofit")) {
        RetrofitBuilder(
            get(),
            get()
        ).build(BASE_URL)
    }

}

val viewModuleFragment = module {
    viewModel { MainViewModel(get()) }
}

val useCase = module {
    single { GetCoinUseCase(get()) }
}

val dataSourceModule = module {
    single<DataSource> {
        RemoteDataSource(
            apiService = get(named("retrofit"))
        )
    }
}

val repositoryModule = module {
    single { CoinRepository(get()) }
}

val module =
    listOf(
        appModule,
        networkModule,
        useCase,
        dataSourceModule,
        repositoryModule,
        viewModuleFragment
    )
