package com.example.bitcoin.presentation.ui.main

import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.bitcoin.R
import com.example.bitcoin.base.BaseFragment
import com.example.bitcoin.databinding.MainFragmentBinding
import com.example.bitcoin.domain.entity.CoinResponse.Data.Coin
import com.example.bitcoin.domain.entity.PageRequest
import com.example.bitcoin.domain.entity.ResultHolder.*
import com.example.bitcoin.extensions.afterTextChanged
import com.example.bitcoin.extensions.gone
import com.example.bitcoin.extensions.runDelayedOnUiThread
import com.example.bitcoin.extensions.visible
import com.example.bitcoin.utils.AppScrollListener
import org.jetbrains.anko.support.v4.toast
import org.koin.androidx.viewmodel.ext.android.viewModel
import java.util.*

class MainFragment : BaseFragment<MainFragmentBinding>() {

    override fun getLayoutResId(): Int {
        return R.layout.main_fragment
    }

    private val mainViewModel by viewModel<MainViewModel>()
    private lateinit var coinAdapter: CoinAdapter
    private var coinList = mutableListOf<Coin>()
    private var coinResponse: MutableList<Coin>? = null
    private lateinit var scrollListener: AppScrollListener
    private lateinit var linearLayoutManager: LinearLayoutManager
    private var currentOffset: Int = 0
    private var isBackup = false
    private var isSearch = false

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
            observerLiveData()
            updateListener()
            initRecyclerView()
            getCoin()
    }

    private fun updateListener() {
        binding.swipeRefresh.setOnRefreshListener {
            binding.edtSearch.setText("")
            coinAdapter.clearItems()
            currentOffset = 0
            getCoin()
        }

        binding.edtSearch.apply {
            afterTextChanged {
                if (!isBackup) {
                    isBackup = true
                    coinList.clear()
                    coinList.addAll(coinAdapter.coinData)
                }
                when (it) {
                    "" -> {
                        isBackup = false
                        isSearch = false
                        scrollListener.setLoaded()
                        coinAdapter.clearItems()
                        setDataList(coinList)
                    }
                    else -> filterCoin(it)
                }
            }
        }
    }

    private fun filterCoin(word: String) {
        isSearch = true
        coinList.filter {
            it.name.lowercase(Locale.ENGLISH).contains(word.lowercase(Locale.ENGLISH))
        }.also {
            coinAdapter.clearItems()
            setDataList(it.toMutableList())
        }
    }

    private fun getCoin() {
        mainViewModel.getCoin(
            pageRequest = PageRequest(
                offset = currentOffset,
                limit = 10
            )
        )
    }

    private fun initRecyclerView() {
        coinAdapter = CoinAdapter().apply {
            setHasStableIds(true)
        }
        linearLayoutManager = LinearLayoutManager(context)
        binding.recyclerView.apply {
            setUpRcv(
                rcv = this,
                adapter = coinAdapter,
                orientation = RecyclerView.VERTICAL,
                colorDivider = R.color.color_gray_line,
                showLastDivider = true,
                layoutManager = linearLayoutManager
            )
        }

        scrollListener = AppScrollListener(linearLayoutManager)
        scrollListener.setOnLoadMoreListener(object : AppScrollListener.OnLoadMoreListener {
            override fun onLoadMore() {
                if (!isSearch) {
                    runDelayedOnUiThread(800) {
                        showLoading(true)
                        currentOffset += 10
                        getCoin()
                    }
                }
            }

            override fun onScrollStateChanged(stateChange: Boolean) {}
        })
        binding.recyclerView.addOnScrollListener(scrollListener)
    }

    private fun setDataList(coinList: MutableList<Coin>?) {
        coinList?.let { coinAdapter.addItems(it) }
        binding.swipeRefresh.isRefreshing = false
    }

    private fun observerLiveData() {
        mainViewModel.coinResponse.observe(viewLifecycleOwner) { result ->
            when(result) {
                is Success -> {
                    result.data?.data?.coins?.let {
                        coinResponse = it
                        setDataList(coinResponse)
                    } ?: run {
                        toast("Empty Data")
                    }

                    if (scrollListener.getLoaded()) {
                        showLoading(false)
                        scrollListener.setLoaded()
                    }
                }
                is Failure -> {
                    binding.emptyView.error().setOnClickListener {
                        getCoin()
                    }.show()
                    binding.swipeRefresh.isRefreshing = false
                }
                is Loading -> {}
            }
        }
    }

    private fun showLoading(isShow: Boolean) {
        if (isShow) {
            binding.layoutLoading.visible(animate = true)
        } else {
            binding.layoutLoading.gone(animate = true)
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        viewModelStore.clear()
    }

}
