package com.example.bitcoin.presentation.ui.main.viewholder

import com.example.bitcoin.base.BaseViewHolder
import com.example.bitcoin.databinding.ItemBitcoinBinding
import com.example.bitcoin.databinding.ItemLoadingBinding
import com.example.bitcoin.domain.entity.CoinResponse
import com.example.bitcoin.extensions.fromHtml

class LoadingViewHolder(binding: ItemLoadingBinding) : BaseViewHolder<ItemLoadingBinding>(
    binding = binding
) {
    fun bind() {
        binding.adapter = this
        binding.executePendingBindings()
    }
}