package com.example.bitcoin.presentation.ui.main

import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import com.example.bitcoin.base.BaseViewModel
import com.example.bitcoin.di.NoConnectivityException
import com.example.bitcoin.domain.entity.CoinResponse
import com.example.bitcoin.domain.entity.PageRequest
import com.example.bitcoin.domain.entity.ResultHolder
import com.example.bitcoin.domain.entity.ResultHolder.Failure
import com.example.bitcoin.domain.entity.ResultHolder.Loading
import com.example.bitcoin.domain.usecase.GetCoinUseCase
import com.hadilq.liveevent.LiveEvent
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class MainViewModel(private val getGetCoinUseCase: GetCoinUseCase) : BaseViewModel() {

    private val _coinResponse = LiveEvent<ResultHolder<CoinResponse>>()
    val coinResponse: LiveData<ResultHolder<CoinResponse>>
        get() = _coinResponse

    fun getCoin(pageRequest: PageRequest) {
        viewModelScope.launch {
            try {
                _coinResponse.value = Loading(true)
                withContext(Dispatchers.IO) {
                    getGetCoinUseCase.getCoin(pageRequest)
                }.apply {
                    _coinResponse.value = this
                }
            } catch (e: NoConnectivityException) {
                _coinResponse.value = Failure(e.message)
            } catch (e: Exception) {
                _coinResponse.value = Failure(e.message)
            }
        }
    }
}

