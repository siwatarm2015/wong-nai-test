package com.example.bitcoin.presentation.ui.main

import android.os.Bundle
import com.example.bitcoin.R
import com.example.bitcoin.base.BaseActivity

class MainActivity : BaseActivity(){

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_activity)
    }

}
