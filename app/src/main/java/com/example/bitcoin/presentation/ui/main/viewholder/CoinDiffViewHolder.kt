package com.example.bitcoin.presentation.ui.main.viewholder

import com.example.bitcoin.base.BaseViewHolder
import com.example.bitcoin.databinding.ItemBitcoinDiffBinding
import com.example.bitcoin.domain.entity.CoinResponse

class CoinDiffViewHolder(binding: ItemBitcoinDiffBinding) : BaseViewHolder<ItemBitcoinDiffBinding>(
    binding = binding
) {
    fun bind(position: Int, coinData: List<CoinResponse.Data.Coin>?) {
        binding.iconUrl = coinData?.get(position)?.iconUrl ?: ""
        binding.name = coinData?.get(position)?.name ?: ""
        binding.adapter = this
        binding.executePendingBindings()
    }
}