package com.example.bitcoin.presentation.ui.main

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.bitcoin.R
import com.example.bitcoin.base.BaseViewHolder
import com.example.bitcoin.databinding.ItemBitcoinBinding
import com.example.bitcoin.databinding.ItemBitcoinDiffBinding
import com.example.bitcoin.domain.entity.CoinResponse.Data.Coin
import com.example.bitcoin.presentation.ui.main.viewholder.CoinDiffViewHolder
import com.example.bitcoin.presentation.ui.main.viewholder.CoinViewHolder
import com.example.bitcoin.utils.VIEW_TYPE_DIFF
import com.example.bitcoin.utils.VIEW_TYPE_NORMAL

class CoinAdapter : RecyclerView.Adapter<BaseViewHolder<*>>() {

    var coinData = mutableListOf<Coin>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder<*> {
        val inflater = LayoutInflater.from(parent.context)
        return when (viewType) {
            VIEW_TYPE_NORMAL -> {
                val binding: ItemBitcoinBinding = DataBindingUtil.inflate(
                    inflater,
                    R.layout.item_bitcoin,
                    parent,
                    false
                )
                CoinViewHolder(binding)
            }
            VIEW_TYPE_DIFF -> {
                val binding: ItemBitcoinDiffBinding = DataBindingUtil.inflate(
                    inflater,
                    R.layout.item_bitcoin_diff,
                    parent,
                    false
                )
                CoinDiffViewHolder(binding)
            }
            else -> {
                throw IllegalArgumentException("Invalid view type")
            }
        }
    }

    override fun getItemCount(): Int = coinData.size

    override fun getItemViewType(position: Int): Int {
        return when {
            position.plus(1) % 5 == 0 -> {
                VIEW_TYPE_DIFF
            }
            else -> {
                VIEW_TYPE_NORMAL
            }
        }
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun onBindViewHolder(holder: BaseViewHolder<*>, position: Int) {
        return when (holder) {
            is CoinViewHolder -> {
                holder.bind(position = position, coinData = coinData)
            }
            is CoinDiffViewHolder -> {
                holder.bind(position = position, coinData = coinData)
            }
            else -> {
                throw IllegalArgumentException()
            }
        }
    }

    fun addItems(coinItems: MutableList<Coin>) {
        coinData.addAll(coinItems)
        notifyDataSetChanged()
    }

    fun clearItems(){
        coinData.clear()
        notifyDataSetChanged()
    }
}