package com.example.bitcoin.presentation.ui.main.viewholder

import com.example.bitcoin.base.BaseViewHolder
import com.example.bitcoin.databinding.ItemBitcoinBinding
import com.example.bitcoin.domain.entity.CoinResponse
import com.example.bitcoin.extensions.fromHtml

class CoinViewHolder(binding: ItemBitcoinBinding) : BaseViewHolder<ItemBitcoinBinding>(
    binding = binding
) {
    fun bind(position: Int, coinData: List<CoinResponse.Data.Coin>?) {
        binding.iconUrl = coinData?.get(position)?.iconUrl ?: ""
        binding.name = coinData?.get(position)?.name ?: ""
        binding.description =
            coinData?.get(position)?.description?.fromHtml() ?: ""
        binding.adapter = this
        binding.executePendingBindings()
    }
}