package com.example.bitcoin.utils

import android.graphics.drawable.Drawable
import android.os.Build
import android.widget.ImageView
import androidx.annotation.DrawableRes
import androidx.databinding.BindingAdapter
import coil.ImageLoader
import coil.decode.GifDecoder
import coil.decode.ImageDecoderDecoder
import coil.decode.SvgDecoder
import coil.load
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.request.target.Target
import com.example.bitcoin.R


object BindingUtils {

    @JvmStatic
    @BindingAdapter(value = ["loadImageUrl", "placeholderRes"], requireAll = false)
    fun glideLoadImage(
        view: ImageView,
        url: String?,
        @DrawableRes placeholderRes: Int = R.drawable.ic_launcher_background
    ) {
        val options = RequestOptions()
            .placeholder(placeholderRes)
            .error(placeholderRes)
            .fallback(placeholderRes)

        Glide.with(view.context)
            .load(url)
            .apply(options)
            .listener(object : RequestListener<Drawable> {
                override fun onLoadFailed(
                    e: GlideException?,
                    model: Any?,
                    target: Target<Drawable>?,
                    isFirstResource: Boolean
                ): Boolean {
                    view.setBackgroundResource(R.drawable.ic_launcher_background)
                    return false
                }

                override fun onResourceReady(
                    resource: Drawable?,
                    model: Any?,
                    target: Target<Drawable>?,
                    dataSource: DataSource?,
                    isFirstResource: Boolean
                ): Boolean {
                    view.background = null
                    return false
                }
            })
            .into(view)
    }

    @JvmStatic
    @BindingAdapter(value = ["loadImageSVGUrl", "placeholderRes"], requireAll = false)
    fun glideLoadSvg(
        view: ImageView,
        url: String?,
        @DrawableRes placeholderRes: Int = R.drawable.ic_launcher_background
    ) {
        val imageLoader = ImageLoader.Builder(view.context)
            .components {
                if (Build.VERSION.SDK_INT >= 28) {
                    add(ImageDecoderDecoder.Factory())
                } else {
                    add(GifDecoder.Factory())
                }
                add(SvgDecoder.Factory())
            }
            .build()
        view.load(url,imageLoader){
            placeholder(placeholderRes)
        }
    }
}