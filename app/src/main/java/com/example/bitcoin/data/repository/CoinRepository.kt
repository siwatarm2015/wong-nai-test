package com.example.bitcoin.data.repository

import com.example.bitcoin.data.datasource.DataSource
import com.example.bitcoin.domain.entity.CoinResponse
import com.example.bitcoin.domain.entity.PageRequest
import com.example.bitcoin.domain.entity.ResultHolder

class CoinRepository(
    private val remoteDataSource: DataSource
) {
    suspend fun getCoin(
        pageRequest: PageRequest
    ): ResultHolder<CoinResponse> {
        return remoteDataSource.getCoin(
            pageRequest = pageRequest
        )
    }

}
