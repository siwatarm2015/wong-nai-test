package com.example.bitcoin.data.datasource

import com.example.bitcoin.di.NoConnectivityException
import com.example.bitcoin.domain.entity.ResultHolder
import com.example.bitcoin.data.service.ApiService
import com.example.bitcoin.domain.entity.CoinResponse
import com.example.bitcoin.domain.entity.PageRequest
import com.example.bitcoin.domain.entity.ResultHolder.*
import com.example.bitcoin.testing.OpenForTesting

@OpenForTesting
class RemoteDataSource(private val apiService: ApiService) : DataSource {
    override suspend fun getCoin(
        pageRequest: PageRequest
    ): ResultHolder<CoinResponse> {
        val response = apiService.getCoin(
            offset = pageRequest.offset,
            limit = pageRequest.limit
        )
        return try {
            if (response.isSuccessful) {
                Success(response.body())
            } else {
                Failure(response.message())
            }
        } catch (throwable: NoConnectivityException) {
            Failure(throwable.message)
        }
    }
}



