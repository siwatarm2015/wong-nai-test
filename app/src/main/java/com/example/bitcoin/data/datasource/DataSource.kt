package com.example.bitcoin.data.datasource

import com.example.bitcoin.domain.entity.CoinResponse
import com.example.bitcoin.domain.entity.PageRequest
import com.example.bitcoin.domain.entity.ResultHolder

interface DataSource {
    suspend fun getCoin(
        pageRequest: PageRequest
    ): ResultHolder<CoinResponse>
}