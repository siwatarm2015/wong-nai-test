package com.example.bitcoin.data.service

import com.example.bitcoin.domain.entity.CoinResponse
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

/**
 * REST API access points
 */

interface ApiService {
    @GET("coins")
    suspend fun getCoin(
        @Query("offset") offset: Int,
        @Query("limit") limit: Int = 10,
    ): Response<CoinResponse>
}