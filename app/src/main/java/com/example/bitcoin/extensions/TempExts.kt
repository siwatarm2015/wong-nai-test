package com.example.bitcoin.extensions

fun Float.toFahrenheit() : Float {
    val c = this * 9/ 5 + 32
    val f = String.format("%.02f", c)
    return f.toFloat()
}

fun Float.toCelsius() : Float {
    val f = this - 32
    val c = f * 5 / 9
    return String.format("%.02f", c).toFloat()
}

