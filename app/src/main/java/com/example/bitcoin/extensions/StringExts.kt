package com.example.bitcoin.extensions

import android.annotation.SuppressLint
import android.os.Build
import android.text.Html
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

fun String.formatDate() : String {
    var format = SimpleDateFormat(this.searchFormat(), Locale.ENGLISH)
    val newDate = format.parse(this)
    format = SimpleDateFormat("dd MMM yyyy", Locale.ENGLISH)
    return format.format(newDate ?: Calendar.getInstance().time)
}

fun String.fromHtml(): String {
    return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
        Html.fromHtml(this, Html.FROM_HTML_SEPARATOR_LINE_BREAK_DIV).toString().trim()
    } else {
        Html.fromHtml(this).toString().trim()
    }
}

@SuppressLint("SimpleDateFormat")
fun String?.searchFormat(): String {
    val formats = arrayOf(
        "yyyy-MM-dd'T'HH:mm:ss'Z'",
        "yyyy-MM-dd'T'HH:mm:ssZ", "yyyy-MM-dd'T'HH:mm:ss",
        "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", "yyyy-MM-dd'T'HH:mm:ss.SSSZ",
        "yyyy-MM-dd HH:mm:ss", "MM/dd/yyyy HH:mm:ss",
        "MM/dd/yyyy'T'HH:mm:ss.SSS'Z'", "MM/dd/yyyy'T'HH:mm:ss.SSSZ",
        "MM/dd/yyyy'T'HH:mm:ss.SSS", "MM/dd/yyyy'T'HH:mm:ssZ",
        "MM/dd/yyyy'T'HH:mm:ss", "yyyy:MM:dd HH:mm:ss",
        "yyyyMMdd", "yyyy MM dd'T'HH:mm:ss'Z'",
        "yyyy MM dd'T'HH:mm:ssZ", "yyyy MM dd'T'HH:mm:ss",
        "yyyy MM dd'T'HH:mm:ss.SSS'Z'",
        "yyyy MM dd'T'HH:mm:ss.SSSZ", "yyyy MM dd HH:mm:ss",
        "dd-MM-yyyy'T'HH:mm:ss'Z'", "dd-MM-yyyy'T'HH:mm:ssZ",
        "dd-MM-yyyy'T'HH:mm:ss", "dd-MM-yyyy'T'HH:mm:ss.SSS'Z'",
        "dd-MM-yyyy'T'HH:mm:ss.SSSZ", "dd-MM-yyyy HH:mm:ss",
        "dd/MM/yyyy HH:mm:ss", "dd/MM/yyyy'T'HH:mm:ss.SSS'Z'",
        "dd/MM/yyyy'T'HH:mm:ss.SSSZ", "dd/MM/yyyy'T'HH:mm:ss.SSS",
        "dd/MM/yyyy'T'HH:mm:ssZ", "dd/MM/yyyy'T'HH:mm:ss",
        "dd:MM:yyyy HH:mm:ss", "ddMMyyyy",
        "dd MM yyyy'T'HH:mm:ss'Z'", "dd MM yyyy'T'HH:mm:ssZ",
        "dd MM yyyy'T'HH:mm:ss", "dd MM yyyy'T'HH:mm:ss.SSS'Z'",
        "dd MM yyyy'T'HH:mm:ss.SSSZ", "dd MM yyyy HH:mm:ss",
        "EEE MMM dd yyyy HH:mm:ss GMT'Z' (UTC)")

    if (this != null) {
        for (parse in formats) {
            try {
                val sdf = SimpleDateFormat(parse, Locale.ENGLISH)
                sdf.parse(this)
                return parse
            } catch (e: ParseException) {
            } catch (r: IllegalArgumentException) {
            }
        }
    } else {
        return "Not found format"
    }
    return this
}