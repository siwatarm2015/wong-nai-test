package com.example.bitcoin.domain.entity

data class PageRequest(
    val offset: Int = 0,
    val limit: Int = 10
)