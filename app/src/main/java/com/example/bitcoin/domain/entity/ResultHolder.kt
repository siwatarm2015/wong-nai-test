package com.example.bitcoin.domain.entity

/**
 * A generic class that holds a value with its loading status.
 * @param <T>
</T> */
sealed class ResultHolder<out T : Any> {
    data class Success<T : Any>(val data: T?) : ResultHolder<T>()
    data class Failure(val error: String?,val code: Int? = 0): ResultHolder<Nothing>()
    data class Loading(val isLoad: Boolean) : ResultHolder<Nothing>()
}
