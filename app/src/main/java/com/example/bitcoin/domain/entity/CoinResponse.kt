package com.example.bitcoin.domain.entity


import com.google.gson.annotations.SerializedName

data class CoinResponse(
    @SerializedName("data")
    val `data`: Data? = Data(),
    @SerializedName("status")
    val status: String = ""
) {
    data class Data(
        @SerializedName("base")
        val base: Base = Base(),
        @SerializedName("coins")
        val coins: MutableList<Coin> = mutableListOf(),
        @SerializedName("stats")
        val stats: Stats = Stats()
    ) {
        data class Base(
            @SerializedName("sign")
            val sign: String = "",
            @SerializedName("symbol")
            val symbol: String = ""
        )

        data class Coin(
            @SerializedName("allTimeHigh")
            val allTimeHigh: AllTimeHigh = AllTimeHigh(),
            @SerializedName("approvedSupply")
            val approvedSupply: Boolean = false,
            @SerializedName("change")
            val change: Double = 0.0,
            @SerializedName("circulatingSupply")
            val circulatingSupply: Double = 0.0,
            @SerializedName("color")
            val color: String = "",
            @SerializedName("confirmedSupply")
            val confirmedSupply: Boolean = false,
            @SerializedName("description")
            val description: String = "",
            @SerializedName("firstSeen")
            val firstSeen: Long = 0,
            @SerializedName("history")
            val history: List<String> = listOf(),
            @SerializedName("iconType")
            val iconType: String = "",
            @SerializedName("iconUrl")
            val iconUrl: String = "",
            @SerializedName("id")
            val id: Int = 0,
            @SerializedName("links")
            val links: List<Link> = listOf(),
            @SerializedName("listedAt")
            val listedAt: Int = 0,
            @SerializedName("marketCap")
            val marketCap: Long = 0,
            @SerializedName("name")
            val name: String = "",
            @SerializedName("numberOfExchanges")
            val numberOfExchanges: Double = 0.0,
            @SerializedName("numberOfMarkets")
            val numberOfMarkets: Double = 0.0,
            @SerializedName("penalty")
            val penalty: Boolean = false,
            @SerializedName("price")
            val price: String = "",
            @SerializedName("rank")
            val rank: Int = 0,
            @SerializedName("slug")
            val slug: String = "",
            @SerializedName("socials")
            val socials: List<Social> = listOf(),
            @SerializedName("symbol")
            val symbol: String = "",
            @SerializedName("totalSupply")
            val totalSupply: Double = 0.0,
            @SerializedName("type")
            val type: String = "",
            @SerializedName("uuid")
            val uuid: String = "",
            @SerializedName("volume")
            val volume: Long = 0,
            @SerializedName("websiteUrl")
            val websiteUrl: String? = null
        ) {
            data class AllTimeHigh(
                @SerializedName("price")
                val price: String = "",
                @SerializedName("timestamp")
                val timestamp: Long = 0
            )

            data class Link(
                @SerializedName("name")
                val name: String = "",
                @SerializedName("type")
                val type: String = "",
                @SerializedName("url")
                val url: String = ""
            )

            data class Social(
                @SerializedName("name")
                val name: String = "",
                @SerializedName("type")
                val type: String = "",
                @SerializedName("url")
                val url: String = ""
            )
        }

        data class Stats(
            @SerializedName("base")
            val base: String = "",
            @SerializedName("limit")
            val limit: Int = 0,
            @SerializedName("offset")
            val offset: Int = 0,
            @SerializedName("order")
            val order: String = "",
            @SerializedName("total")
            val total: Int = 0,
            @SerializedName("total24hVolume")
            val total24hVolume: Double = 0.0,
            @SerializedName("totalExchanges")
            val totalExchanges: Int = 0,
            @SerializedName("totalMarketCap")
            val totalMarketCap: Double = 0.0,
            @SerializedName("totalMarkets")
            val totalMarkets: Int = 0
        )
    }
}