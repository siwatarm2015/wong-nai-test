package com.example.bitcoin.domain.usecase

import com.example.bitcoin.data.repository.CoinRepository
import com.example.bitcoin.domain.entity.PageRequest

class GetCoinUseCase(private val coinRepository: CoinRepository) {

    suspend fun getCoin(
        pageRequest: PageRequest
    ) = coinRepository.getCoin(
        pageRequest = pageRequest
    )
}