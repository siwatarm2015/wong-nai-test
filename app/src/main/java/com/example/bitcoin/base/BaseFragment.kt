package com.example.bitcoin.base

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.LiveData
import androidx.navigation.NavArgs
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.SimpleItemAnimator
import com.example.bitcoin.R
import com.yqritc.recyclerviewflexibledivider.HorizontalDividerItemDecoration


abstract class BaseFragment<T : ViewDataBinding> : Fragment() {

    lateinit var mActivity: FragmentActivity

    @LayoutRes
    abstract fun getLayoutResId(): Int

    protected lateinit var binding: T

    protected open val navController by lazy {
        findNavController()
    }

    inline fun <reified A : NavArgs> args(): A {
        return navArgs<A>().value
    }

    inline fun <reified T> LiveData<T>.observe(crossinline onNext: (T) -> Unit) {
        observe(this@BaseFragment) { onNext(it) }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mActivity = requireActivity()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return DataBindingUtil.inflate<T>(inflater, getLayoutResId(), container, false)
            .apply { binding = this }.root
    }

    inline fun <reified T : Activity> Activity.startActivity() {
        startActivity(Intent(this, T::class.java))
    }

    fun <VH : RecyclerView.ViewHolder> setUpRcv(
        rcv: RecyclerView,
        adapter:
        RecyclerView.Adapter<VH>,
        isNestedScrollingEnabled: Boolean = false,
        orientation: Int = RecyclerView.VERTICAL,
        colorDivider: Int?,
        layoutManager: LinearLayoutManager = LinearLayoutManager(context),
        showLastDivider: Boolean,
        changeAnimation: Boolean = false
    ) {
        rcv.setHasFixedSize(true)
        (rcv.itemAnimator as SimpleItemAnimator).supportsChangeAnimations = changeAnimation
        layoutManager.orientation = orientation
        rcv.layoutManager = layoutManager
        rcv.isNestedScrollingEnabled = isNestedScrollingEnabled
        if (colorDivider != null) {
            if (orientation == LinearLayoutManager.VERTICAL) {
                val config = HorizontalDividerItemDecoration
                    .Builder(context)
                    .sizeResId(R.dimen.divide_line)
                    .color(ContextCompat.getColor(requireContext(), colorDivider)).apply {
                        if (showLastDivider) this.showLastDivider()
                    }.run {
                        this.build()
                    }
                rcv.addItemDecoration(config)
            }
        }
        rcv.adapter = adapter
    }

}