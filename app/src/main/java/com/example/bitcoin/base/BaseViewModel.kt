package com.example.bitcoin.base

import androidx.lifecycle.ViewModel
import androidx.annotation.CallSuper
import kotlinx.coroutines.*
import kotlin.coroutines.CoroutineContext

/**
 * ViewModel for Rx Jobs
 *
 * launch() - launch a Rx request
 * clear all request on stop
 */
abstract class BaseViewModel : ViewModel(), CoroutineScope {

    private val job = Job()

    override val coroutineContext: CoroutineContext
        get() = job + Dispatchers.Main + SupervisorJob()

    @CallSuper
    override fun onCleared() {
        job.cancel()
        coroutineContext.cancel()
        super.onCleared()
    }
}