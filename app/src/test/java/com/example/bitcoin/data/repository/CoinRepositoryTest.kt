package com.example.bitcoin.data.repository

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.example.bitcoin.data.datasource.DataSource
import com.example.bitcoin.domain.entity.CoinResponse
import com.example.bitcoin.domain.entity.PageRequest
import com.example.bitcoin.domain.entity.ResultHolder.Success
import com.example.bitcoin.utils.TestCoroutineRule
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.times
import com.nhaarman.mockitokotlin2.verify
import io.mockk.mockk
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.junit.Assert
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import org.junit.runner.RunWith
import org.mockito.Mockito.`when`
import org.mockito.Mockito.mock
import org.mockito.junit.MockitoJUnitRunner

@ExperimentalCoroutinesApi
@RunWith(MockitoJUnitRunner::class)
class CoinRepositoryTest {

    @get:Rule
    val testInstantTaskExecutorRule: TestRule = InstantTaskExecutorRule()

    @get:Rule
    val testCoroutineRule = TestCoroutineRule()

    private val remoteDataSource = mock(DataSource::class.java)

    private val dataRepo = CoinRepository(remoteDataSource)

    @Test
    fun `getCoin - success`() {
        testCoroutineRule.runBlockingTest {
            `when`(
                remoteDataSource.getCoin(
                    pageRequest = any()
                )
            ).thenReturn(
                Success(
                    data = CoinResponse(
                        data = CoinResponse.Data(
                            coins = mutableListOf(mockk<CoinResponse.Data.Coin>())
                        ),
                        status = "success"
                    )
                )
            )
            val result = dataRepo.getCoin(
                pageRequest = PageRequest(offset = 0, limit = 10)
            ) as Success
            verify(remoteDataSource, times(1)).getCoin(
                pageRequest = any()
            )
            Assert.assertEquals("success", result.data?.status)
            Assert.assertNotNull(result.data?.data?.coins)
        }
    }

    @Test
    fun `getCoin - fail with over limit`() {
        testCoroutineRule.runBlockingTest {
            `when`(
                remoteDataSource.getCoin(
                    pageRequest = PageRequest(
                        offset = 0,
                        limit = 100*10000
                    )
                )
            ).thenReturn(
                Success(
                    data = CoinResponse(
                        data = null,
                        status = "fail"
                    )
                )
            )
            val result = dataRepo.getCoin(
                pageRequest = PageRequest(offset = 0, limit = 100*10000)
            ) as Success
            verify(remoteDataSource, times(1)).getCoin(
                pageRequest = PageRequest(
                    offset = 0,
                    limit = 100*10000
                )
            )
            Assert.assertEquals("fail", result.data?.status)
            Assert.assertNull(result.data?.data)
        }
    }
}