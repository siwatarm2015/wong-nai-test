# Android Application

Use Coroutines & Retrofit

## Technology Stack
	1. ViewModel
	2. Koin Di for Kotlin
	3. LiveData
	4. Coroutines
	5. AndroidX
**Architecture**

MVVM + Clean

## Here are some of the screenshots.

![picture](https://bitbucket.org/siwatarm2015/wong-nai-test/raw/6b9c3a2d271d0a37962047560134f9ab01f08ca3/screenshot/1.jpg)

![picture](https://bitbucket.org/siwatarm2015/wong-nai-test/raw/6b9c3a2d271d0a37962047560134f9ab01f08ca3/screenshot/2.jpg)


Any PRs or issues are really welcome.